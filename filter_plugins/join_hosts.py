#!/usr/bin/python

class FilterModule(object):
  def filters(self):
    return {'join_hosts': join_hosts}

def join_hosts(hosts, hostnames, port):
  myHosts = [];
  
  for idx, host in enumerate(hosts):
    myHosts.append(hostnames[idx] + '=' + 'http://' + host + ':' + port)
  return myHosts
