#!/usr/bin/python

class FilterModule(object):
  def filters(self):
    return {'sort_ip': sort_ip}

def sort_ip(a):
  # a_list = [x.strip() for x in a.split(',')]
  key = sorted(a, key=lambda ip: long(''.join(["%02X" % long(i) for i in ip.split('.')]), 16))
  return key